﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace Homework_Event_Manager
{
    public partial class Calendar : Form
    {

       // bool display = false;
        string titleBox, discriptionBox;
        List<Event> events = new List<Event>();
        DateTime startDate = new DateTime(), endDate = new DateTime();
        string sDate, sMonth, sYear;
        string eDate, eMonth, eYear;
        /*bool viewappointments = true;
        int count;
        string tDate,
          tMonth ,
          tYear ;*/

        ArrayList ODate = new ArrayList();
        ArrayList EDate = new ArrayList();
        ArrayList INote = new ArrayList();
        ArrayList FDiscript = new ArrayList();
        public Calendar()
        {
            InitializeComponent();
            read_List();
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            if (!search_Mode.Checked)
            {
                // Will not allow to make a appointment on past
                if ((monthCalendar.SelectionStart < monthCalendar.TodayDate) && (monthCalendar.SelectionEnd < monthCalendar.TodayDate))
                {
                    startDate = monthCalendar.TodayDate;

                    sDate = monthCalendar.TodayDate.Day.ToString();
                    sMonth = monthCalendar.TodayDate.Month.ToString();
                    sYear = monthCalendar.TodayDate.Year.ToString();

                    endDate = monthCalendar.TodayDate;

                    eDate = monthCalendar.TodayDate.Day.ToString();
                    eMonth = monthCalendar.TodayDate.Month.ToString();
                    eYear = monthCalendar.TodayDate.Year.ToString();
                }
                else if ((monthCalendar.SelectionStart < monthCalendar.TodayDate) && (monthCalendar.SelectionEnd >= monthCalendar.TodayDate))
                {
                    startDate = monthCalendar.TodayDate;

                    sDate = monthCalendar.TodayDate.Day.ToString();
                    sMonth = monthCalendar.TodayDate.Month.ToString();
                    sYear = monthCalendar.TodayDate.Year.ToString();

                    endDate = monthCalendar.SelectionEnd;

                    eDate = monthCalendar.SelectionEnd.Day.ToString();
                    eMonth = monthCalendar.SelectionEnd.Month.ToString();
                    eYear = monthCalendar.SelectionEnd.Year.ToString();

                }
                else
                {
                    startDate = monthCalendar.SelectionStart;

                    sDate = monthCalendar.SelectionStart.Day.ToString();
                    sMonth = monthCalendar.SelectionStart.Month.ToString();
                    sYear = monthCalendar.SelectionStart.Year.ToString();

                    endDate = monthCalendar.SelectionEnd;

                    eDate = monthCalendar.SelectionEnd.Day.ToString();
                    eMonth = monthCalendar.SelectionEnd.Month.ToString();
                    eYear = monthCalendar.SelectionEnd.Year.ToString();
                }
            }
            else
            {
                startDate = monthCalendar.SelectionStart;

                sDate = monthCalendar.SelectionStart.Day.ToString();
                sMonth = monthCalendar.SelectionStart.Month.ToString();
                sYear = monthCalendar.SelectionStart.Year.ToString();

                endDate = monthCalendar.SelectionEnd;

                eDate = monthCalendar.SelectionEnd.Day.ToString();
                eMonth = monthCalendar.SelectionEnd.Month.ToString();
                eYear = monthCalendar.SelectionEnd.Year.ToString();
            }

           

            /*ODate.Add(sMonth + sDate + sYear);
            EDate.Add(eDate + eYear + eMonth); */

            startBox.Text = sMonth + "/" + sDate + "/" + sYear;
            endBox.Text = eMonth + "/" + eDate + "/" + eYear;


            UpdateEventList();
        }

        private void eventTitle_TextChanged(object sender, EventArgs e)
        {
            titleBox = eventTitle.Text.ToString();
            INote.Add(eventTitle.Text.ToString());
        }

        private void discriptionBox_TextChanged(object sender, EventArgs e)
        {
            discriptionBox = eventDiscription.Text.ToString();
            FDiscript.Add(eventDiscription.Text.ToString());
        }
        

      /*  private void dissss()
        {

            if (display)
            {
                for (int i = 0; i < count; i++)
                {

                    //allEventBox.Text = allEventBox.Text + storage[i] + "\r\n";
                }
            }
            display = false;
        }*/

        public void save_Event()
        {
            DeleteEvent();//Too prevent duplicate event names
            startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, startTimeHour.SelectedIndex, startTimeMinutes.SelectedIndex, 0);
            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, endTimeHour.SelectedIndex, endTimeMinute.SelectedIndex, 0);
            Event temp = new Event(eventTitle.Text, eventDiscription.Text, startDate, endDate);

            /*
            // Handles bolded dates for the Calendar
            if ((startDate.Year == endDate.Year) && (startDate.Month == endDate.Month) && (startDate.Day == endDate.Day))
            {
                monthCalendar.AddBoldedDate(startDate);
            }
            else {
                DateTime[] eventDates = {startDate, endDate};
                monthCalendar.BoldedDates = eventDates;
            }

            monthCalendar.UpdateBoldedDates();
            */

            GetBoldedDates();

            events.Add(temp);

            UpdateEventList();
        }

        public void DeleteEvent() //Removes events from the calendar
        {
            Event temp = null;
            foreach (Event item in events)
            {
                if (item.GetName() == eventTitle.Text)
                {
                    temp = item;
                }
            }


            if(temp != null)events.Remove(temp);

            GetBoldedDates();

            UpdateEventList();
        }

        private void UpdateEventList()
        {
            listBox1.Items.Clear();
            foreach (Event item2 in events)
            {
                if (item2.GetStart().CompareTo(monthCalendar.SelectionEnd) <= 0 && item2.GetEnd().CompareTo(monthCalendar.SelectionStart) >= 0) listBox1.Items.Add(item2.GetName());
            }
        }

     /*   private void finding()
        {
            

                tDate = monthCalendar.SelectionStart.Day.ToString();
                tMonth = monthCalendar.SelectionStart.Month.ToString();
                tYear = monthCalendar.SelectionStart.Year.ToString();
            

            for (int i = 0; i < ODate.Count; i++)
            {
                string value = ODate[i] as String;

                if ((tMonth + tDate + tYear) == value)
                {

                    //timePicker.Text = timePicker.Text + storage[i] + "\r\n";

                }
            }
        }*/

        private void addEvent_Click(object sender, EventArgs e)
        {
            if (!search_Mode.Checked)
            {
                
                save_Event();
                save_List(); //------------ Save to txt file -----
                clearingEverything();
                monthCalendar.Refresh();

                GetBoldedDates();
            }
            else
            {
                listBox1.Items.Clear();
                find_app();
            }
            
        }

        private void GetBoldedDates()
        {
            if (!search_Mode.Checked)
            {
                monthCalendar.RemoveAllBoldedDates();
                foreach (Event items in events)
                {

                    DateTime temp = new DateTime(items.GetStart().Year, items.GetStart().Month, items.GetStart().Day, items.GetStart().Hour, items.GetStart().Minute, items.GetStart().Second);
                    while (temp.CompareTo(items.GetEnd()) <= 0)
                    {
                        monthCalendar.AddBoldedDate(temp);
                        temp = temp.AddDays(1);
                    }

                    monthCalendar.AddBoldedDate(items.GetEnd().Date);
                    monthCalendar.UpdateBoldedDates();
                }
            }
        }

        public void clearingEverything()
        {
            titleBox = null;
            discriptionBox = null;
            eventTitle.Clear();
            eventDiscription.Clear();
            startBox.Clear();
            endBox.Clear();
        }


        private void startTimeHour_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void startTimeMinutes_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private class Event
        {
            public String name;
            private String description;
            private DateTime startTime;
            private DateTime endTime;

            public Event(String name, String description, DateTime startTime, DateTime endTime)
            {
                this.name = name;
                this.description = description;
                this.startTime = startTime;
                this.endTime = endTime;
            }

            public void SetName(String name){this.name = name;}
            public String GetName(){return name;}

            public void SetDesc(String description){this.description = description;}
            public String GetDesc(){return description;}

            public void SetStart(DateTime startTime){this.startTime = startTime;}
            public DateTime GetStart(){return startTime;}

            public void SetEnd(DateTime endTime){this.endTime = endTime;}
            public DateTime GetEnd(){return endTime;}
        }

        private void startBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void endBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (Event items in events)
            {
                if (items.GetName() == (string)listBox1.SelectedItem)
                {
                    eventTitle.Text = items.GetName();
                    eventDiscription.Text = items.GetDesc();



                    startBox.Text = items.GetStart().Month.ToString() + "/" + items.GetStart().Day.ToString() + "/" + items.GetStart().Year.ToString();
                    startTimeHour.SelectedIndex = items.GetStart().Hour;
                    startTimeMinutes.SelectedIndex = items.GetStart().Minute;

                    endBox.Text = items.GetEnd().Month.ToString() + "/" + items.GetEnd().Day.ToString() + "/" + items.GetEnd().Year.ToString();
                    endTimeHour.SelectedIndex = items.GetEnd().Hour;
                    endTimeMinute.SelectedIndex = items.GetEnd().Minute;
                }
            }
        }

        private void Delete_Event_Click(object sender, EventArgs e)
        {
            if (!search_Mode.Checked)
            {
                DeleteEvent();
                save_List();
                clearingEverything();
            }
        }


        public void save_List()
        {
            //Gets the path to the 'MyDocuments' folder to be used for output
            string myDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string outputFile = myDocuments + "\\EventFile.dat";

            //Saves the data from the calendar into the 'EventFile.dat' file
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(outputFile))
            {
                foreach (Event items in events)
                {
                    file.WriteLine(items.GetName().ToString() + "|" +
                                   items.GetDesc().ToString() + "|" +
                                   items.GetStart().Year.ToString() + "|" + items.GetStart().Month.ToString() + "|" + items.GetStart().Day.ToString() + "|" +
                                   items.GetStart().Hour.ToString() + "|" +
                                   items.GetStart().Minute.ToString() + "|" +
                                   items.GetEnd().Year.ToString() + "|" + items.GetEnd().Month.ToString() + "|" + items.GetEnd().Day.ToString() + "|" +
                                   items.GetEnd().Hour.ToString() + "|" +
                                   items.GetEnd().Minute.ToString());
                }
            }
        }

        public void read_List()
        {
            //Gets the path to the 'MyDocuments' folder.
            string myDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string inputFile = myDocuments + "\\EventFile.dat";

            //Checks to see if the 'EventFile.dat' file exists
            //If it does not exist, it creates a file
            //If it does exist, it reads the file in
            if (!File.Exists(inputFile))
            {
                StreamWriter sw = File.CreateText(inputFile);
            }
            else {
                String line;
                //-----------------Change Path to Save at proper place --------------------------------------
                System.IO.StreamReader file = new System.IO.StreamReader(inputFile);
                while ((line = file.ReadLine()) != null)
                {
                    string[] words = line.Split('|'); //System.Text.RegularExpressions.Regex.Split(line, "	");
                    startDate = new DateTime(int.Parse(words[2]), int.Parse(words[3]), int.Parse(words[4]), int.Parse(words[5]), int.Parse(words[6]), 0);
                    endDate = new DateTime(int.Parse(words[7]), int.Parse(words[8]), int.Parse(words[9]), int.Parse(words[10]), int.Parse(words[11]), 0);
                    Event temp = new Event(words[0], words[1], startDate, endDate);

                    events.Add(temp);

                    listBox1.Items.Add(temp.GetName());

                }

                file.Close();
                System.Console.ReadLine();

                GetBoldedDates();
                UpdateEventList();
            }
            

            
            
        }

        private void search_Mode_CheckedChanged(object sender, EventArgs e)
        {
            if (!search_Mode.Checked)
            {
                listBox1.Items.Clear();
                clearingEverything();
                read_List();
                addEvent.Text = "Save Event";
                Delete_Event.Text = "Delete Event";
                
            }
            else
            {
                clearingEverything();
                listBox1.Items.Clear();
                eventTitle.ReadOnly = true;
                eventDiscription.ReadOnly = true;
                endBox.ReadOnly = true;
                endTimeHour.Visible = false;
                endTimeMinute.Visible = false;
                addEvent.Text = "Search";
                Delete_Event.Text = "";
            }
            
        }

        public void find_app()
        {
            
            if (startDate == null || endDate == null)
                {
                    startDate = monthCalendar.TodayDate;
                    endDate = monthCalendar.TodayDate;
                }

            foreach (Event items in events)
            {
                if(items.GetStart().Date >= startDate)
                    listBox1.Items.Add(items.GetName());
            }
 
        }

        private void printButton_Click(object sender, EventArgs e)
        {
            //Reading
            //Gets the path to the 'MyDocuments' folder.
            string myDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string inputFile = myDocuments + "\\EventFile.dat";
            string outputFile = myDocuments + "\\HomeworkReport.txt";

            //Checks to see if the 'EventFile.dat' file exists
            //If it does not exist, it creates a file
            //If it does exist, it reads the file in

            if (!File.Exists(inputFile))
            {
                StreamWriter sw = File.CreateText(inputFile);
            }
            else
            {
                String line;
                //-----------------Change Path to Save at proper place --------------------------------------
                System.IO.StreamReader file = new System.IO.StreamReader(inputFile);
                using (System.IO.StreamWriter files = new System.IO.StreamWriter(outputFile))
                {
                    files.WriteLine("Start Date and Time         End Date and Time           Title           Detail");
                    while ((line = file.ReadLine()) != null)
                    {

                        string[] words = line.Split('|'); //System.Text.RegularExpressions.Regex.Split(line, "	");
                        files.WriteLine(words[2] + "/" + words[3] + "/" + words[4] + "-" + words[5] + ":" + words[6] + "\t\t\t"
                            + words[7] + "/" + words[8] + "/" + words[9] + "-" + words[10] + ":" + words[11] + "\t\t\t"
                            + words[0] + "\t\t\t" + words[1]);

                    }
                }
            }
            Process.Start(outputFile);
        }
    }
}
