﻿namespace Homework_Event_Manager
{
    partial class Calendar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.monthCalendar = new System.Windows.Forms.MonthCalendar();
            this.eventTitle = new System.Windows.Forms.TextBox();
            this.eventDiscription = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.addEvent = new System.Windows.Forms.Button();
            this.startTimeLabel = new System.Windows.Forms.Label();
            this.endTimeLabel = new System.Windows.Forms.Label();
            this.startTimeHour = new System.Windows.Forms.ComboBox();
            this.startTimeMinutes = new System.Windows.Forms.ComboBox();
            this.endTimeHour = new System.Windows.Forms.ComboBox();
            this.endTimeMinute = new System.Windows.Forms.ComboBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Delete_Event = new System.Windows.Forms.Button();
            this.endBox = new System.Windows.Forms.TextBox();
            this.startBox = new System.Windows.Forms.TextBox();
            this.search_Mode = new System.Windows.Forms.CheckBox();
            this.printButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // monthCalendar
            // 
            this.monthCalendar.Location = new System.Drawing.Point(13, 15);
            this.monthCalendar.Margin = new System.Windows.Forms.Padding(7);
            this.monthCalendar.MaxSelectionCount = 21;
            this.monthCalendar.Name = "monthCalendar";
            this.monthCalendar.TabIndex = 0;
            this.monthCalendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // eventTitle
            // 
            this.eventTitle.Location = new System.Drawing.Point(72, 186);
            this.eventTitle.Margin = new System.Windows.Forms.Padding(2);
            this.eventTitle.Name = "eventTitle";
            this.eventTitle.Size = new System.Drawing.Size(169, 20);
            this.eventTitle.TabIndex = 7;
            this.eventTitle.TextChanged += new System.EventHandler(this.eventTitle_TextChanged);
            // 
            // eventDiscription
            // 
            this.eventDiscription.Location = new System.Drawing.Point(72, 213);
            this.eventDiscription.Margin = new System.Windows.Forms.Padding(2);
            this.eventDiscription.Multiline = true;
            this.eventDiscription.Name = "eventDiscription";
            this.eventDiscription.Size = new System.Drawing.Size(169, 90);
            this.eventDiscription.TabIndex = 8;
            this.eventDiscription.TextChanged += new System.EventHandler(this.discriptionBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 186);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Event Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 216);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Event Details";
            // 
            // addEvent
            // 
            this.addEvent.Location = new System.Drawing.Point(14, 373);
            this.addEvent.Margin = new System.Windows.Forms.Padding(2);
            this.addEvent.Name = "addEvent";
            this.addEvent.Size = new System.Drawing.Size(77, 23);
            this.addEvent.TabIndex = 13;
            this.addEvent.Text = "Save Event";
            this.addEvent.UseVisualStyleBackColor = true;
            this.addEvent.Click += new System.EventHandler(this.addEvent_Click);
            // 
            // startTimeLabel
            // 
            this.startTimeLabel.AutoSize = true;
            this.startTimeLabel.Location = new System.Drawing.Point(14, 307);
            this.startTimeLabel.Name = "startTimeLabel";
            this.startTimeLabel.Size = new System.Drawing.Size(55, 13);
            this.startTimeLabel.TabIndex = 15;
            this.startTimeLabel.Text = "Start Time";
            // 
            // endTimeLabel
            // 
            this.endTimeLabel.AutoSize = true;
            this.endTimeLabel.Location = new System.Drawing.Point(17, 332);
            this.endTimeLabel.Name = "endTimeLabel";
            this.endTimeLabel.Size = new System.Drawing.Size(52, 13);
            this.endTimeLabel.TabIndex = 16;
            this.endTimeLabel.Text = "End Time";
            // 
            // startTimeHour
            // 
            this.startTimeHour.FormattingEnabled = true;
            this.startTimeHour.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
            this.startTimeHour.Location = new System.Drawing.Point(161, 308);
            this.startTimeHour.Name = "startTimeHour";
            this.startTimeHour.Size = new System.Drawing.Size(38, 21);
            this.startTimeHour.TabIndex = 17;
            this.startTimeHour.Text = "00";
            this.startTimeHour.SelectedIndexChanged += new System.EventHandler(this.startTimeHour_SelectedIndexChanged);
            // 
            // startTimeMinutes
            // 
            this.startTimeMinutes.FormattingEnabled = true;
            this.startTimeMinutes.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59"});
            this.startTimeMinutes.Location = new System.Drawing.Point(205, 308);
            this.startTimeMinutes.Name = "startTimeMinutes";
            this.startTimeMinutes.Size = new System.Drawing.Size(36, 21);
            this.startTimeMinutes.TabIndex = 18;
            this.startTimeMinutes.Text = "00";
            this.startTimeMinutes.SelectedIndexChanged += new System.EventHandler(this.startTimeMinutes_SelectedIndexChanged);
            // 
            // endTimeHour
            // 
            this.endTimeHour.FormattingEnabled = true;
            this.endTimeHour.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
            this.endTimeHour.Location = new System.Drawing.Point(161, 331);
            this.endTimeHour.Name = "endTimeHour";
            this.endTimeHour.Size = new System.Drawing.Size(38, 21);
            this.endTimeHour.TabIndex = 19;
            this.endTimeHour.Text = "00";
            // 
            // endTimeMinute
            // 
            this.endTimeMinute.FormattingEnabled = true;
            this.endTimeMinute.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59"});
            this.endTimeMinute.Location = new System.Drawing.Point(205, 331);
            this.endTimeMinute.Name = "endTimeMinute";
            this.endTimeMinute.Size = new System.Drawing.Size(36, 21);
            this.endTimeMinute.TabIndex = 20;
            this.endTimeMinute.Text = "00";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(251, 41);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(174, 355);
            this.listBox1.TabIndex = 21;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(251, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Events:";
            // 
            // Delete_Event
            // 
            this.Delete_Event.Location = new System.Drawing.Point(156, 373);
            this.Delete_Event.Name = "Delete_Event";
            this.Delete_Event.Size = new System.Drawing.Size(85, 23);
            this.Delete_Event.TabIndex = 23;
            this.Delete_Event.Text = "Delete Event";
            this.Delete_Event.UseVisualStyleBackColor = true;
            this.Delete_Event.Click += new System.EventHandler(this.Delete_Event_Click);
            // 
            // endBox
            // 
            this.endBox.Location = new System.Drawing.Point(72, 332);
            this.endBox.Margin = new System.Windows.Forms.Padding(2);
            this.endBox.Name = "endBox";
            this.endBox.Size = new System.Drawing.Size(84, 20);
            this.endBox.TabIndex = 5;
            this.endBox.TextChanged += new System.EventHandler(this.endBox_TextChanged);
            // 
            // startBox
            // 
            this.startBox.Location = new System.Drawing.Point(72, 307);
            this.startBox.Margin = new System.Windows.Forms.Padding(2);
            this.startBox.Name = "startBox";
            this.startBox.Size = new System.Drawing.Size(84, 20);
            this.startBox.TabIndex = 4;
            this.startBox.TextChanged += new System.EventHandler(this.startBox_TextChanged);
            // 
            // search_Mode
            // 
            this.search_Mode.AutoSize = true;
            this.search_Mode.Location = new System.Drawing.Point(82, 400);
            this.search_Mode.Margin = new System.Windows.Forms.Padding(2);
            this.search_Mode.Name = "search_Mode";
            this.search_Mode.Size = new System.Drawing.Size(87, 17);
            this.search_Mode.TabIndex = 24;
            this.search_Mode.Text = "SearchMode";
            this.search_Mode.UseVisualStyleBackColor = true;
            this.search_Mode.Visible = false;
            this.search_Mode.CheckedChanged += new System.EventHandler(this.search_Mode_CheckedChanged);
            // 
            // printButton
            // 
            this.printButton.Location = new System.Drawing.Point(350, 10);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(75, 23);
            this.printButton.TabIndex = 25;
            this.printButton.Text = "Print";
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // Calendar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 409);
            this.Controls.Add(this.printButton);
            this.Controls.Add(this.search_Mode);
            this.Controls.Add(this.Delete_Event);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.endTimeMinute);
            this.Controls.Add(this.endTimeHour);
            this.Controls.Add(this.startTimeMinutes);
            this.Controls.Add(this.startTimeHour);
            this.Controls.Add(this.endTimeLabel);
            this.Controls.Add(this.startTimeLabel);
            this.Controls.Add(this.addEvent);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.eventDiscription);
            this.Controls.Add(this.eventTitle);
            this.Controls.Add(this.endBox);
            this.Controls.Add(this.startBox);
            this.Controls.Add(this.monthCalendar);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Calendar";
            this.Text = "Calendar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar monthCalendar;
        private System.Windows.Forms.TextBox eventTitle;
        private System.Windows.Forms.TextBox eventDiscription;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button addEvent;
        private System.Windows.Forms.Label startTimeLabel;
        private System.Windows.Forms.Label endTimeLabel;
        private System.Windows.Forms.ComboBox startTimeHour;
        private System.Windows.Forms.ComboBox startTimeMinutes;
        private System.Windows.Forms.ComboBox endTimeHour;
        private System.Windows.Forms.ComboBox endTimeMinute;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Delete_Event;
        private System.Windows.Forms.TextBox endBox;
        private System.Windows.Forms.TextBox startBox;
        private System.Windows.Forms.CheckBox search_Mode;
        private System.Windows.Forms.Button printButton;
    }
}

