﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace Homework_Manager
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        bool display = false;
        string titleBox, discriptionBox;
        bool startDateClick = false;
        bool endDateClick = false;
        string[] storage = new string [100];
        string sDate, sMonth, sYear;
        string eDate, eMonth, eYear;
        bool viewappointments = true;
        int count;
        string tDate = null,
          tMonth = null,
          tYear = null;

        ArrayList ODate = new ArrayList();
        ArrayList EDate = new ArrayList();
        ArrayList INote = new ArrayList();
        ArrayList FDiscript = new ArrayList();
        
        private void monthCalendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            if(startDateClick)
            {
                sDate = monthCalendar.SelectionStart.Day.ToString();
                sMonth = monthCalendar.SelectionStart.Month.ToString();
                sYear = monthCalendar.SelectionStart.Year.ToString();

                ODate.Add(sDate+sMonth+sYear);

                startDate.Text = monthCalendar.SelectionStart.ToString();
                
            } 
            else if (endDateClick)
            {
                eDate = monthCalendar.SelectionStart.Day.ToString();
                eMonth = monthCalendar.SelectionStart.Month.ToString();
                eYear = monthCalendar.SelectionStart.Year.ToString();

                EDate.Add(sDate+sMonth+sYear);

                endDate.Text = monthCalendar.SelectionStart.ToString();

            }
          /*  else if (viewappointments)
            {
                tDate = monthCalendar.SelectionStart.Day.ToString();
                tMonth = monthCalendar.SelectionStart.Month.ToString();
                tYear = monthCalendar.SelectionStart.Year.ToString();
            }*/  
        }

        private void startDate_Click(object sender, EventArgs e)
        {
            startDateClick = true;
            endDateClick = false;
            viewappointments = false;
        }

        private void endDate_Click(object sender, EventArgs e)
        {
            endDateClick = true;
            startDateClick = false;
            viewappointments = false;
        }

        private void eventTitle_TextChanged(object sender, EventArgs e)
        {
            titleBox = eventTitle.Text.ToString();
            INote.Add(eventTitle.Text.ToString());
        }

        private void eventDiscription_TextChanged(object sender, EventArgs e)
        {
            discriptionBox = eventDiscription.Text.ToString();
            FDiscript.Add(eventDiscription.Text.ToString());
        }

        private void AddEvent_Click(object sender, EventArgs e)
        {
            store_Data();
            
            clearingEverything();
        }

        public void store_Data()
        {

            
            storage[count] = sMonth +"/" + sDate +"/" + sYear + " TO "
                           + eMonth + "/" + eDate + "/" + eYear + "    "
                           + titleBox + "   " + discriptionBox;

            count++;
            //dissss();
           
        }

        public void clearingEverything()
        {
            titleBox = null;
            discriptionBox = null;
            startDateClick = false;
            endDateClick = false;
            eventTitle.Clear();
            eventDiscription.Clear();
            startDate.Clear();
            endDate.Clear();
        }



        private void displayEvent_Click(object sender, EventArgs e)
        {
            display = true;
            textBox1.Clear();
            dissss();
        }

        private void dissss()
        {
            
          if(display)
          {
              for (int i = 0; i < count; i++)
              {
                
                  textBox1.Text = textBox1.Text + storage[i] + "\r\n";
              }
         }
          display = false;
        }

        private void finding()
        {
           /* if(viewappointments)
            {*/
                resBox.Text = "blah blah";
                tDate = monthCalendar.SelectionStart.Day.ToString();
                tMonth = monthCalendar.SelectionStart.Month.ToString();
                tYear = monthCalendar.SelectionStart.Year.ToString();
          //  }
           

            for (int i = 0; i < ODate.Count; i++)
            {
                string value = ODate[i] as String;
        
                if ((tMonth + tDate + tYear) == value)
                {

                    resBox.Text = resBox.Text + storage[i] + "\r\n";
                    
                }
            }
        }

        private void viewAppointments_button_Click(object sender, EventArgs e)
        {
            finding();
            viewappointments = false;
        }

    }
}
