﻿namespace Homework_Manager
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.monthCalendar = new System.Windows.Forms.MonthCalendar();
            this.button1 = new System.Windows.Forms.Button();
            this.eventTitle = new System.Windows.Forms.TextBox();
            this.eventDiscription = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.startDate = new System.Windows.Forms.TextBox();
            this.endDate = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.resBox = new System.Windows.Forms.TextBox();
            this.viewAppointments_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // monthCalendar
            // 
            this.monthCalendar.Font = new System.Drawing.Font("Modern No. 20", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monthCalendar.Location = new System.Drawing.Point(18, 18);
            this.monthCalendar.Name = "monthCalendar";
            this.monthCalendar.TabIndex = 0;
            this.monthCalendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar_DateChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(513, 244);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Add Event";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.AddEvent_Click);
            // 
            // eventTitle
            // 
            this.eventTitle.Location = new System.Drawing.Point(513, 96);
            this.eventTitle.Name = "eventTitle";
            this.eventTitle.Size = new System.Drawing.Size(200, 22);
            this.eventTitle.TabIndex = 3;
            this.eventTitle.TextChanged += new System.EventHandler(this.eventTitle_TextChanged);
            // 
            // eventDiscription
            // 
            this.eventDiscription.Location = new System.Drawing.Point(513, 141);
            this.eventDiscription.Multiline = true;
            this.eventDiscription.Name = "eventDiscription";
            this.eventDiscription.Size = new System.Drawing.Size(200, 84);
            this.eventDiscription.TabIndex = 4;
            this.eventDiscription.TextChanged += new System.EventHandler(this.eventDiscription_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(510, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Event_Title";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(510, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Event_Discription";
            // 
            // startDate
            // 
            this.startDate.Location = new System.Drawing.Point(513, 18);
            this.startDate.Name = "startDate";
            this.startDate.Size = new System.Drawing.Size(200, 22);
            this.startDate.TabIndex = 9;
            // 
            // endDate
            // 
            this.endDate.Location = new System.Drawing.Point(513, 51);
            this.endDate.Name = "endDate";
            this.endDate.Size = new System.Drawing.Size(200, 22);
            this.endDate.TabIndex = 10;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(414, 17);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(93, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Start Date";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.startDate_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(414, 51);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(93, 23);
            this.button3.TabIndex = 12;
            this.button3.Text = "End Date";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.endDate_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(35, 244);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(93, 23);
            this.button4.TabIndex = 13;
            this.button4.Text = "Display";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.displayEvent_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(35, 287);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(678, 201);
            this.textBox1.TabIndex = 14;
            // 
            // resBox
            // 
            this.resBox.Location = new System.Drawing.Point(780, 51);
            this.resBox.Multiline = true;
            this.resBox.Name = "resBox";
            this.resBox.Size = new System.Drawing.Size(363, 437);
            this.resBox.TabIndex = 16;
            // 
            // viewAppointments_button
            // 
            this.viewAppointments_button.Location = new System.Drawing.Point(780, 22);
            this.viewAppointments_button.Name = "viewAppointments_button";
            this.viewAppointments_button.Size = new System.Drawing.Size(169, 23);
            this.viewAppointments_button.TabIndex = 18;
            this.viewAppointments_button.Text = "viewAppointment";
            this.viewAppointments_button.UseVisualStyleBackColor = true;
            this.viewAppointments_button.Click += new System.EventHandler(this.viewAppointments_button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1165, 514);
            this.Controls.Add(this.viewAppointments_button);
            this.Controls.Add(this.resBox);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.endDate);
            this.Controls.Add(this.startDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.eventDiscription);
            this.Controls.Add(this.eventTitle);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.monthCalendar);
            this.Name = "Form1";
            this.Text = "HomeworkManager";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

         #endregion

        private System.Windows.Forms.MonthCalendar monthCalendar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox eventTitle;
        private System.Windows.Forms.TextBox eventDiscription;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox startDate;
        private System.Windows.Forms.TextBox endDate;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox resBox;
        private System.Windows.Forms.Button viewAppointments_button;
    }
}

